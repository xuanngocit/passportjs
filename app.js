const express = require('express');
const authRouter = require('./routes/auth-routes');
const passportSetup = require('./config/passport-setup')

const mongoose = require('mongoose');
const keys = require('./config/keys');
const app = express();  

//Thiep lap view
app.set('view engine','ejs');
// ket noi mongodb
mongoose.connect(keys.mongodb.dbURL,{socketTimeoutMS: 0,
    keepAlive: true,
    reconnectTries: 30},()=>{
    console.log('ket noi mongodb')
})
//thiet lap routes
app.use('/auth',authRouter);
//Thiet lap route home
app.get('/',(req,res)=>{
    res.render('home');
})



app.listen(3000,()=>{
    console.log('listening on port 3000');
})